﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PomodoroApp {
    public partial class Form1 : Form {
        int seconds;
        bool inWork;

        public Form1() {
            InitializeComponent();
            seconds = 25 * 60;
            inWork = true;
        }


        private void btnStartStop_Click(object sender, EventArgs e) {
            //timer1.Enabled = true;
            timer1.Enabled = !timer1.Enabled;
        }

        private void btnReset_Click(object sender, EventArgs e) {
            timer1.Enabled = false;
            try {
                SetSecondsAndWork(int.Parse(tbWork.Text) * 60, true);
            }
            catch (FormatException) {
                MessageBox.Show("Upisali ste neispravnu vrijednost!");
            }
            UpdateStatus();
        }

        private void timer1_Tick(object sender, EventArgs e) {
            seconds--;
            if(seconds == 0) {
                if(inWork) {
                    SetSecondsAndWork(5 * 60, false);
                }
                else {
                    SetSecondsAndWork(25 * 60, true); ;
                }
            }
            UpdateStatus();
        }

        private void UpdateStatus() {
            lblStatus.Text = string.Format("{0}:{1}", seconds / 60, seconds % 60);
        }

        private void SetSecondsAndWork (int seconds, bool inWork) {
            this.seconds = seconds;
            this.inWork = inWork;
        }
    }
}
