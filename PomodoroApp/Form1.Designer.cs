﻿
using System;

namespace PomodoroApp
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblWork = new System.Windows.Forms.Label();
            this.lblRest = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.tbWork = new System.Windows.Forms.TextBox();
            this.tbRest = new System.Windows.Forms.TextBox();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.btnRest = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // lblWork
            // 
            this.lblWork.AutoSize = true;
            this.lblWork.Location = new System.Drawing.Point(94, 56);
            this.lblWork.Name = "lblWork";
            this.lblWork.Size = new System.Drawing.Size(35, 20);
            this.lblWork.TabIndex = 0;
            this.lblWork.Text = "Rad";
            // 
            // lblRest
            // 
            this.lblRest.AutoSize = true;
            this.lblRest.Location = new System.Drawing.Point(409, 56);
            this.lblRest.Name = "lblRest";
            this.lblRest.Size = new System.Drawing.Size(56, 20);
            this.lblRest.TabIndex = 1;
            this.lblRest.Text = "Odmor";
            // 
            // lblStatus
            // 
            this.lblStatus.Font = new System.Drawing.Font("Segoe UI", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblStatus.Location = new System.Drawing.Point(64, 200);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(417, 82);
            this.lblStatus.TabIndex = 2;
            this.lblStatus.Text = "25:00";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbWork
            // 
            this.tbWork.Location = new System.Drawing.Point(64, 96);
            this.tbWork.Name = "tbWork";
            this.tbWork.Size = new System.Drawing.Size(100, 27);
            this.tbWork.TabIndex = 3;
            this.tbWork.Text = "25";
            // 
            // tbRest
            // 
            this.tbRest.Location = new System.Drawing.Point(381, 96);
            this.tbRest.Name = "tbRest";
            this.tbRest.Size = new System.Drawing.Size(100, 27);
            this.tbRest.TabIndex = 4;
            this.tbRest.Text = "5";
            // 
            // btnStartStop
            // 
            this.btnStartStop.Location = new System.Drawing.Point(64, 332);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(105, 57);
            this.btnStartStop.TabIndex = 5;
            this.btnStartStop.Text = "Start / Stop";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
            // 
            // btnRest
            // 
            this.btnRest.Location = new System.Drawing.Point(376, 332);
            this.btnRest.Name = "btnRest";
            this.btnRest.Size = new System.Drawing.Size(105, 57);
            this.btnRest.TabIndex = 6;
            this.btnRest.Text = "Reset";
            this.btnRest.UseVisualStyleBackColor = true;
            this.btnRest.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 495);
            this.Controls.Add(this.btnRest);
            this.Controls.Add(this.btnStartStop);
            this.Controls.Add(this.tbRest);
            this.Controls.Add(this.tbWork);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblRest);
            this.Controls.Add(this.lblWork);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.Label lblWork;
        private System.Windows.Forms.Label lblRest;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.TextBox tbWork;
        private System.Windows.Forms.TextBox tbRest;
        private System.Windows.Forms.Button btnStartStop;
        private System.Windows.Forms.Button btnRest;
        private System.Windows.Forms.Timer timer1;
    }
}

